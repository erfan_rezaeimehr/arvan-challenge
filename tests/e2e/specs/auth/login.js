describe('/login', () => {
  beforeEach(() => {
    cy.visit('/login')
  })

  it('shows Login', () => {
    cy.contains('h2', 'Login')
  })

  it('links to register', () => {
    cy.contains('a', 'Register Now').should('have.attr', 'href', '/register')
  })

  it('requires email', () => {
    cy.get('form')
      .contains('Login')
      .click()

    cy.get('form #email')
      .siblings('.text-red-600')
      .should('contain', 'Required field')
  })

  it('requires valid email', () => {
    cy.get('form #email').type('thisIsInvalidEmail{enter}')

    cy.get('form #email')
      .siblings('.text-red-600')
      .should('contain', 'Invalid email')
  })

  it('navigates to /articles on successful login', () => {
    cy.get('form #email').type('erfan.rezaee71@gmail.com')
    cy.get('form #password').type('erfan7748{enter}')

    cy.url().should('include', '/articles')

    cy.get('main h2').should('contain', 'All Posts')
  })
})
