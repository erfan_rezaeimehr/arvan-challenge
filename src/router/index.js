import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/auth/Login'
import Register from '../views/auth/Register'
import Articles from '../views/article/Articles'
import ArticleCreate from '../views/article/ArticleCreate'
import ArticleEdit from '../views/article/ArticleEdit'
import NotFound from '../views/NotFound'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: { name: 'Articles' },
  },
  {
    path: '/articles',
    name: 'Articles',
    component: Articles,
    meta: { requiresAuth: true },
  },
  {
    path: '/articles/page/1',
    redirect: { name: 'Articles' },
  },
  {
    path: '/articles/page/:pageNumber',
    name: 'ArticlesWithPage',
    component: Articles,
    meta: { requiresAuth: true },
    beforeEnter(routeTo, routeFrom, next) {
      // Show 404 page for invalid routes like: /articles/page/-1 or /articles/page/blahblah
      const pageNumber = parseInt(routeTo.params.pageNumber)
      if (pageNumber <= 0 || isNaN(pageNumber))
        return next({ name: 'NotFound' })

      next()
    },
  },
  {
    path: '/articles/edit/:slug',
    name: 'ArticleEdit',
    component: ArticleEdit,
    meta: { requiresAuth: true },
  },
  {
    path: '/articles/create',
    name: 'ArticleCreate',
    component: ArticleCreate,
    meta: { requiresAuth: true },
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: { requiresVisitor: true },
  },
  {
    path: '/register',
    name: 'Register',
    component: Register,
    meta: { requiresVisitor: true },
  },
  {
    path: '/404',
    name: 'NotFound',
    component: NotFound,
  },
  {
    path: '*',
    redirect: { name: 'NotFound' },
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})

router.beforeEach((to, from, next) => {
  const loggedIn = localStorage.getItem('user')

  // If user isn't logged in, and wants to go to routes with `requiresAuth` meta
  // Redirect to login
  if (to.matched.some(record => record.meta.requiresAuth) && !loggedIn) {
    next({ name: 'Login' })
  }

  // If user is already logged in and wants to go to login or register pages
  // Redirect to the Dashboard (Articles page)
  if (to.matched.some(record => record.meta.requiresVisitor) && loggedIn) {
    next({ name: 'Articles' })
  } else {
    next()
  }
})

export default router
