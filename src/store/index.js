import Vue from 'vue'
import Vuex from 'vuex'
import auth from './modules/auth'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isSideMenuOpen: false,
  },

  getters: {},

  mutations: {
    TOGGLE_SIDE_MENU(state) {
      state.isSideMenuOpen = !state.isSideMenuOpen
    },
    CLOSE_SIDE_MENU(state) {
      state.isSideMenuOpen = false
    },
  },

  actions: {
    toggleSideMenu({ commit }) {
      commit('TOGGLE_SIDE_MENU')
    },
  },

  modules: {
    auth,
  },
})
