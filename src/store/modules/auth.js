import AuthService from '../../services/AuthService'

const state = {
  user: JSON.parse(localStorage.getItem('user')) || null,
}

const getters = {
  loggedIn(state) {
    return state.user !== null
  },
}

const mutations = {
  SET_USER_DATA(state, { user }) {
    localStorage.setItem('user', JSON.stringify(user))
    state.user = user
  },
  LOGOUT() {
    localStorage.removeItem('user')
    location.reload()
  },
}

const actions = {
  register({ commit }, credentials) {
    return AuthService.register(credentials).then(({ data }) => {
      commit('SET_USER_DATA', data)
    })
  },
  login({ commit }, credentials) {
    return AuthService.login(credentials).then(({ data }) => {
      commit('SET_USER_DATA', data)
    })
  },
  logout({ commit, getters }) {
    if (getters.loggedIn) commit('LOGOUT')
  },
}

export default {
  state,
  getters,
  mutations,
  actions,
}
