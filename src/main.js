import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { errorHandlingMixin } from './mixin/errorHandlingMixin'
import './assets/css/tailwind.css'
import Vuelidate from 'vuelidate'
import Notifications from 'vue-notification'
import BaseButton from './components/ui/BaseButton'
import './assets/css/styles.scss'

Vue.component('BaseButton', BaseButton)

Vue.use(Vuelidate)
Vue.use(Notifications)

Vue.mixin(errorHandlingMixin)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app')
