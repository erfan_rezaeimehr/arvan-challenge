export const errorHandlingMixin = {
  methods: {
    showErrorsWithNotification(errors) {
      errors.forEach((error) => {
        this.$notify({
          group: 'app',
          text: error.charAt(0).toUpperCase() + error.slice(1), // Capitalize first letter of error text
          type: 'error',
          duration: 10000,
        })
      })
    },
  },
}
