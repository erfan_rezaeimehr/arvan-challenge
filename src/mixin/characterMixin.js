export const characterMixin = {
  methods: {
    removeNonAscii(str) {
      if (str === null || str === '') return false
      str = str.toString()
      return str.replace(/[^\x20-\x7E]/g, '')
    },

    removeNonAsciiFromArray(arr) {
      const removedNonAsciiArray = []
      arr.forEach((item) => {
        if (this.removeNonAscii(item).trim()) {
          removedNonAsciiArray.push(this.removeNonAscii(item).trim())
        }
      })

      return removedNonAsciiArray
    },
  },
}
