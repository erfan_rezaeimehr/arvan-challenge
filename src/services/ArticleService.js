import Api from './Api'

export default {
  getArticles(author, perPage, offset) {
    return Api.get(
      `/articles?author=${author}&limit=${perPage}&offset=${offset}`
    )
  },
  getArticle(slug) {
    return Api.get(`/articles/${slug}`)
  },
  deleteArticle(slug) {
    return Api.delete(`/articles/${slug}`)
  },
  createArticle(article) {
    return Api.post('/articles', article)
  },
  updateArticle(slug, article) {
    return Api.put(`/articles/${slug}`, article)
  },
  getTags() {
    return Api.get('/tags')
  },
}
