import axios from 'axios'

const DEFAULT_CONFIG = {
  baseURL: `https://conduit.productionready.io/api`,
  withCredentials: false,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
  timeout: 20000,
}

const Api = axios.create(DEFAULT_CONFIG)

Api.interceptors.request.use(function(config) {
  const TOKEN = localStorage.getItem('user')
    ? JSON.parse(localStorage.getItem('user')).token
    : null

  if (TOKEN) config.headers.common['Authorization'] = `Token ${TOKEN}`
  return config
})

Api.interceptors.response.use(
  function(response) {
    return response
  },
  function(error) {
    if (!error.response) {
      alert('Network error, Please try later.')
    }

    // Expired or incorrect token
    if (error.response.status === 401) {
      localStorage.removeItem('user')
      window.location.replace('/')
      return
    }

    if (error.response.status === 404) {
      window.location.replace('/404')
    }

    // A flat `errorsArray` to display all the server-side errors with notification
    const errorsArray = []

    const errors = error.response.data.errors
    for (let errorKey in errors) {
      errors[errorKey].forEach(error =>
        errorsArray.push(`${errorKey} ${error}.`)
      )
    }

    return Promise.reject(errorsArray)
  }
)

export default Api
